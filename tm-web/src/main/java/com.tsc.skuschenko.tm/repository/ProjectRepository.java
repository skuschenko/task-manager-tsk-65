package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.model.Project;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

@Getter
@Repository
public class ProjectRepository {

    @NotNull
    private static final ProjectRepository INSTANCE = new ProjectRepository();

    @NotNull
    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        save(new Project("p1"));
        save(new Project("p2"));
    }

    @NotNull
    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    public void clearAll() {
        projects.clear();
    }

    public Project create() {
        @NotNull final Project project =
                new Project("p" + UUID.randomUUID().toString());
        save(project);
        return project;
    }

    @NotNull
    public Collection<Project> findAll() {
        return projects.values();
    }

    @NotNull
    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void remove(@NotNull final Project project) {
        projects.remove(project);
    }

    public void removeById(@Nullable final String id) {
        projects.remove(id);
    }

    public void save(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

}
