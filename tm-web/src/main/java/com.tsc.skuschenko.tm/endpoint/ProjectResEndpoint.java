package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.api.IProjectRestEndpoint;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.repository.ProjectRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
public class ProjectResEndpoint implements IProjectRestEndpoint {

    @NotNull
    static final String CREATE_METHOD = "/create";

    @NotNull
    static final String CREATE_ALL_METHOD = "/createAll";

    @NotNull
    static final String DELETE_BY_ID_METHOD = "/deleteById/{id}";

    @NotNull
    static final String DELETE_ALL_METHOD = "/deleteAll";

    @NotNull
    static final String FIND_BY_ID_METHOD = "/findById/{id}";

    @NotNull
    static final String FIND_ALL_METHOD = "/findAll";

    @NotNull
    static final String SAVE_METHOD = "/save";

    @NotNull
    static final String SAVE_ALL_METHOD = "/saveAll";

    @Autowired
    @NotNull
    private ProjectRepository projectRepository;

    @Override
    @PostMapping(CREATE_METHOD)
    public void create(@RequestBody @NotNull final Project project) {
        projectRepository.save(project);
    }

    @Override
    @PostMapping(CREATE_ALL_METHOD)
    public void createAll(
            @RequestBody @NotNull final Collection<Project> projects
    ) {
        projects.forEach(projectRepository::save);
    }

    @Override
    @DeleteMapping(DELETE_BY_ID_METHOD)
    public void deleteAll(@PathVariable("id") @NotNull final String id) {
        projectRepository.removeById(id);
    }

    @Override
    @DeleteMapping(DELETE_ALL_METHOD)
    public void deleteAll(
            @RequestBody @NotNull final Collection<Project> projects
    ) {
        projects.forEach(item->projectRepository.removeById(item.getId()));
    }

    @Override
    @GetMapping(FIND_BY_ID_METHOD)
    public Project find(@PathVariable("id") @NotNull final String id) {
        return projectRepository.findById(id);
    }

    @Override
    @GetMapping(FIND_ALL_METHOD)
    public Collection<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    @PutMapping(SAVE_METHOD)
    public void save(@RequestBody @NotNull final Project project) {
        projectRepository.save(project);
    }

    @Override
    @PutMapping(SAVE_ALL_METHOD)
    public void saveAll(
            @RequestBody @NotNull final Collection<Project> projects
    ) {
        projects.forEach(projectRepository::save);
    }

}
