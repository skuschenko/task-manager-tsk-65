package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.api.ITaskRestEndpoint;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.repository.TaskRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
public class TaskResEndpoint implements ITaskRestEndpoint {

    @NotNull
    static final String CREATE_METHOD = "/create";

    @NotNull
    static final String CREATE_ALL_METHOD = "/createAll";

    @NotNull
    static final String DELETE_BY_ID_METHOD = "/deleteById/{id}";

    @NotNull
    static final String DELETE_ALL_METHOD = "/deleteAll";

    @NotNull
    static final String FIND_BY_ID_METHOD = "/findById/{id}";

    @NotNull
    static final String FIND_ALL_METHOD = "/findAll";

    @NotNull
    static final String SAVE_METHOD = "/save";

    @NotNull
    static final String SAVE_ALL_METHOD = "/saveAll";

    @Autowired
    @NotNull
    private TaskRepository taskRepository;

    @Override
    @PostMapping(CREATE_METHOD)
    public void create(@RequestBody @NotNull final Task task) {
        taskRepository.save(task);
    }

    @Override
    @PostMapping(CREATE_ALL_METHOD)
    public void createAll(
            @RequestBody @NotNull final Collection<Task> tasks
    ) {
        tasks.forEach(taskRepository::save);
    }

    @Override
    @DeleteMapping(DELETE_BY_ID_METHOD)
    public void deleteAll(@PathVariable("id") @NotNull final String id) {
        taskRepository.removeById(id);
    }

    @Override
    @DeleteMapping(DELETE_ALL_METHOD)
    public void deleteAll(
            @RequestBody @NotNull final Collection<Task> tasks
    ) {
        tasks.forEach(item->taskRepository.removeById(item.getId()));
    }

    @Override
    @GetMapping(FIND_BY_ID_METHOD)
    public Task find(@PathVariable("id") @NotNull final String id) {
        return taskRepository.findById(id);
    }

    @Override
    @GetMapping(FIND_ALL_METHOD)
    public Collection<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    @PutMapping(SAVE_METHOD)
    public void save(@RequestBody @NotNull final Task task) {
        taskRepository.save(task);
    }

    @Override
    @PutMapping(SAVE_ALL_METHOD)
    public void saveAll(
            @RequestBody @NotNull final Collection<Task> tasks
    ) {
        tasks.forEach(taskRepository::save);
    }

}
