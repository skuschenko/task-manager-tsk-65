package com.tsc.skuschenko.tm.api;

import com.tsc.skuschenko.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

public interface ITaskRestEndpoint {


    @PostMapping("/create")
    void create(@RequestBody @NotNull final Task task);

    @PostMapping("/createAll")
    void createAll(
            @RequestBody @NotNull Collection<Task> tasks
    );

    @DeleteMapping("/deleteAll")
    void deleteAll(
            @RequestBody @NotNull Collection<Task> tasks
    );

    @DeleteMapping("/deleteById/{id}")
    void deleteAll(@PathVariable("id") @NotNull String id);

    @GetMapping("/findById/{id}")
    Task find(@PathVariable("id") @NotNull String id);

    @GetMapping("/findAll")
    Collection<Task> findAll();

    @PutMapping("/save")
    void save(@RequestBody @NotNull Task task);

    @PutMapping("/saveAll")
    void saveAll(
            @RequestBody @NotNull Collection<Task> tasks
    );
}
