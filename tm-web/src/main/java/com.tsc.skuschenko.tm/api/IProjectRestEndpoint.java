package com.tsc.skuschenko.tm.api;

import com.tsc.skuschenko.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

public interface IProjectRestEndpoint {


    @PostMapping("/create")
    void create(@RequestBody @NotNull final Project project);

    @PostMapping("/createAll")
    void createAll(@RequestBody @NotNull Collection<Project> projects);

    @DeleteMapping("/deleteAll")
    void deleteAll(@RequestBody @NotNull Collection<Project> projects);

    @DeleteMapping("/deleteById/{id}")
    void deleteAll(@PathVariable("id") @NotNull String id);

    @GetMapping("/findById/{id}")
    Project find(@PathVariable("id") @NotNull String id);

    @GetMapping("/findAll")
    Collection<Project> findAll();

    @PutMapping("/save")
    void save(@RequestBody @NotNull Project project);

    @PutMapping("/saveAll")
    void saveAll(@RequestBody @NotNull Collection<Project> projects);
}
