package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.marker.IntegrationCategory;
import com.tsc.skuschenko.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class TaskRestEndpointTest {

    @Test
    @Category(IntegrationCategory.class)
    public void testChangeStatusById() {

    }

    @NotNull
    static final String API_URL = "http://localhost:8080/api/tasks/";

    @NotNull
    static final String CREATE_METHOD = "create";

    @NotNull
    static final String CREATE_ALL_METHOD = "createAll";

    @NotNull
    static final String DELETE_BY_ID_METHOD = "deleteById/";

    @NotNull
    static final String DELETE_ALL_METHOD = "deleteAll";

    @NotNull
    static final String FIND_BY_ID_METHOD = "findById/";

    @NotNull
    static final String FIND_ALL_METHOD = "findAll";

    @NotNull
    static final String SAVE_METHOD = "save";

    @NotNull
    static final String SAVE_ALL_METHOD = "saveAll";

    @Before
    public void before() {
        deleteAll();
        create();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void create() {
        @NotNull String url = API_URL + CREATE_METHOD;
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final Task task = new Task("pro");
        @NotNull final HttpEntity<Task> requestUpdate =
                new HttpEntity<>(task, headers);
        restTemplate.postForObject(url, requestUpdate, Task.class);
        url = API_URL + FIND_BY_ID_METHOD + task.getId();
        @NotNull final ResponseEntity<Task> response
                = restTemplate.getForEntity(url, Task.class);
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        @Nullable final Task taskNew = response.getBody();
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(task.getId(), taskNew.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void createAll() {
        @NotNull final String url = API_URL + CREATE_ALL_METHOD;
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_ATOM_XML);
        @NotNull final List<Task> tasks = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            tasks.add(new Task("pro" + i));
        }
        @NotNull final HttpEntity<List> requestUpdate =
                new HttpEntity<>(tasks, headers);
        restTemplate.postForObject(url, requestUpdate, List.class);
        @Nullable final List<Task> tasksNew = findAllTasks();
        Assert.assertNotNull(tasksNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        tasksNew.forEach(item -> {
            tasks.forEach((itemOld) -> {
                if (itemOld.getId().equals(item.getId())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), tasks.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteById() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final Task task = findAllTasks().get(0);
        @NotNull final String url =
                API_URL + DELETE_BY_ID_METHOD + task.getId();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity<Task> requestUpdate =
                new HttpEntity<>(task, headers);
        restTemplate.exchange(
                url, HttpMethod.DELETE, requestUpdate, Task.class
        );
        Assert.assertEquals(0, findAllTasks().size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final List<Task> tasks = findAllTasks();
        @NotNull final String url =
                API_URL + DELETE_ALL_METHOD;
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity<Task[]> requestUpdate =
                new HttpEntity<>(tasks.toArray(new Task[0]), headers);
        restTemplate.exchange(
                url, HttpMethod.DELETE, requestUpdate, Task[].class
        );
        Assert.assertEquals(0, findAllTasks().size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findById() {
        @NotNull final List<Task> tasks = findAllTasks();
        Assert.assertNotNull(tasks);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = API_URL + FIND_BY_ID_METHOD +
                tasks.get(0).getId();
        @NotNull final ResponseEntity<Task> response
                = restTemplate.getForEntity(url, Task.class);
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAll() {
        findAllTasks();
    }

    @NotNull
    private List<Task> findAllTasks() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = API_URL + FIND_ALL_METHOD;
        @NotNull final ResponseEntity<List<Task>> response
                = restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Task>>() {
                }
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        Assert.assertNotNull(response.getBody());
        return response.getBody();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void save() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = API_URL + SAVE_METHOD;
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final Task task = findAllTasks().get(0);
        task.setDescription(UUID.randomUUID().toString());
        @NotNull final HttpEntity<Task> requestUpdate =
                new HttpEntity<>(task, headers);
        restTemplate.exchange(
                url, HttpMethod.PUT, requestUpdate, Task.class
        );
        Assert.assertEquals(
                task.getDescription(),
                findAllTasks().get(0).getDescription()
        );
    }

    @Test
    @Category(IntegrationCategory.class)
    public void saveAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = API_URL + SAVE_ALL_METHOD;
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final List<Task> tasks = findAllTasks();
        tasks.forEach(item ->
                item.setDescription(UUID.randomUUID().toString())
        );
        @NotNull final HttpEntity<List<Task>> requestUpdate =
                new HttpEntity<>(tasks, headers);
        restTemplate.exchange(
                url, HttpMethod.PUT, requestUpdate, Task.class
        );
        @Nullable final List<Task> tasksNew = findAllTasks();
        Assert.assertNotNull(tasksNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        tasksNew.forEach(item -> {
            tasks.forEach((itemOld) -> {
                if (itemOld.getDescription().equals(item.getDescription())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), tasks.size());
    }

}
