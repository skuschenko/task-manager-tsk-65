package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.dto.ISessionDTOService;
import com.tsc.skuschenko.tm.api.service.dto.IUserDTOService;
import com.tsc.skuschenko.tm.component.Bootstrap;
import com.tsc.skuschenko.tm.configuration.ServerConfiguration;
import com.tsc.skuschenko.tm.dto.SessionDTO;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.service.dto.SessionDTOService;
import com.tsc.skuschenko.tm.service.dto.UserDTOService;
import com.tsc.skuschenko.tm.util.SignatureUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SessionServiceTest {

    @NotNull
    private static AnnotationConfigApplicationContext context;

    @NotNull
    private static ISessionDTOService sessionService;

    @BeforeClass
    public static void before() {
        context =
                new AnnotationConfigApplicationContext(
                        ServerConfiguration.class
                );
        sessionService = testService();
    }

    @NotNull
    private static ISessionDTOService testService() {
        @NotNull final IConnectionService connectionService =
                context.getBean(ConnectionService.class);
        Assert.assertNotNull(connectionService);
        @NotNull final IUserDTOService userDTOService =
                context.getBean(UserDTOService.class);
        userDTOService.clear();
        userDTOService.create("user1", "user1");
        @NotNull ISessionDTOService sessionService =
                context.getBean(SessionDTOService.class);
        Assert.assertNotNull(sessionService);
        return sessionService;
    }

    @Test
    public void close() {
        @Nullable final SessionDTO session = sessionService.open("user1", "user1");
        Assert.assertNotNull(session);
        sessionService.close(session);
        @Nullable final SessionDTO sessionFind =
                sessionService.getSessionRepository()
                        .findSessionById(session.getId());
        sessionService.close(session);
        Assert.assertNull(sessionFind);
    }

    @Test
    public void open() {
        @NotNull final SessionDTO session = sessionService.open("user1", "user1");
        Assert.assertNotNull(session);
        @Nullable final SessionDTO sessionFind =
                sessionService.getSessionRepository()
                        .findSessionById(session.getId());
        Assert.assertNotNull(sessionFind);
    }

    @Test(expected = AccessForbiddenException.class)
    public void openAccessForbidden() {
        @NotNull final SessionDTO session = sessionService.open("user2", "user2");
        Assert.assertNotNull(session);
        @Nullable final SessionDTO sessionFind =
                sessionService.getSessionRepository()
                        .findSessionById(session.getId());
        Assert.assertNotNull(sessionFind);
    }

    @Test
    public void sign() {
        @NotNull final SessionDTO session = sessionService.open("user1", "user1");
        @Nullable final String signature =
                SignatureUtil.sign(session, "salt", 2565);
        session.setSignature(signature);
        @Nullable final SessionDTO sessionFind =
                sessionService.getSessionRepository()
                        .findSessionById(session.getId());
        Assert.assertNotNull(sessionFind);
        Assert.assertEquals(signature, session.getSignature());
    }

}
