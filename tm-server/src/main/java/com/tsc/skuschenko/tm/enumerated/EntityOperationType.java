package com.tsc.skuschenko.tm.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
@AllArgsConstructor
public enum EntityOperationType {

    POST_LOAD("Post Load"),
    POST_PERSIST("Post Persist"),
    POST_REMOVE("Post Remove"),
    POST_UPDATE("Post Update"),
    PRE_PERSIST("Pre Persist"),
    PRE_REMOVE("Pre Remove"),
    PRE_UPDATE("Pre Update");

    @NotNull
    public final String displayName;

}
