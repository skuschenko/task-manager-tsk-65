package com.tsc.skuschenko.tm.exception.entity.user;

import com.tsc.skuschenko.tm.exception.AbstractException;

public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
