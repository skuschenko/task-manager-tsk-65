package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.api.endpoint.ISessionEndpoint;
import com.tsc.skuschenko.tm.api.service.dto.ISessionDTOService;
import com.tsc.skuschenko.tm.dto.SessionDTO;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@WebService
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @NotNull
    @Autowired
    private ISessionDTOService sessionDTOService;

    @WebMethod
    @Override
    public void closeSession(
            @WebParam(name = "session", partName = "session")
            @NotNull SessionDTO session
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        sessionDTOService.close(session);
    }

    @WebMethod
    @Nullable
    @Override
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login")
            @NotNull final String login,
            @WebParam(name = "password", partName = "password")
            @NotNull final String password
    ) {
        return sessionDTOService.open(login, password);
    }

}
