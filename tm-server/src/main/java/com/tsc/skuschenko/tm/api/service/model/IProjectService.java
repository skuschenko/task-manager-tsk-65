package com.tsc.skuschenko.tm.api.service.model;

import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.model.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    @NotNull
    Project add(
            @Nullable User user, @Nullable String name,
            @Nullable String description
    );

    void addAll(@Nullable List<Project> projects);

    @NotNull
    Project changeStatusById(@Nullable String id, @Nullable Status status);

    @NotNull
    Project changeStatusByIndex(@Nullable Integer index, @Nullable Status status);

    @NotNull
    Project changeStatusByName(@Nullable String name, @Nullable Status status);

    @SneakyThrows
    void clear();

    @NotNull
    Project completeById(@Nullable String id);

    @NotNull
    Project completeByIndex(@Nullable Integer index);

    @NotNull
    Project completeByName(@Nullable String name);

    @Nullable
    List<Project> findAll();

    @NotNull
    List<Project> findAll(@Nullable Comparator<Project> comparator);

    @Nullable
    Project findOneById(@Nullable String id);

    @Nullable
    Project findOneByIndex(Integer index);

    @Nullable
    Project findOneByName(String name);

    @Nullable
    Project removeOneById(String id);

    @Nullable
    Project removeOneByIndex(Integer index);

    @Nullable
    Project removeOneByName(@Nullable String name);

    @NotNull
    Project startById(@Nullable String id);

    @NotNull
    Project startByIndex(@Nullable Integer index);

    @NotNull
    Project startByName(@Nullable String name);

    @NotNull
    Project updateOneById(
            @Nullable String id, @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Project updateOneByIndex(
            @Nullable Integer index, @Nullable String name,
            @Nullable String description
    );

}
