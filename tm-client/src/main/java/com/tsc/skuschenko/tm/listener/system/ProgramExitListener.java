package com.tsc.skuschenko.tm.listener.system;

import com.tsc.skuschenko.tm.event.ConsoleEvent;
import com.tsc.skuschenko.tm.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class ProgramExitListener extends AbstractListener {

    private static final String DESCRIPTION = "exit";

    private static final String NAME = "exit";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@programExitListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.exit(0);
    }

    @Override
    public String name() {
        return NAME;
    }

}
