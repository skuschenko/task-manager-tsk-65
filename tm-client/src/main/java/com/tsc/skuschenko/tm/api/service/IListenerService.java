package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IListenerService {

    void add(AbstractListener abstractCommand);

    @NotNull
    Collection<AbstractListener> getArgs();

    @NotNull
    Collection<AbstractListener> getArguments();

    @NotNull
    Collection<String> getListArgumentName();

    @NotNull
    Collection<String> getListListenerNames();

    @Nullable
    AbstractListener getListenerByArg(@NotNull String name);

    @Nullable
    AbstractListener getListenerByName(@NotNull String name);

    @NotNull
    Collection<AbstractListener> getListeners();

}
